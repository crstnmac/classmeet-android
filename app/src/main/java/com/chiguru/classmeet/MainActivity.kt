@file:Suppress("DEPRECATION")

package com.chiguru.classmeet

import android.Manifest
import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.webkit.*
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val webView: WebView? = null
    private val TAG = "MainActivity"

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        setContentView(R.layout.activity_main)
        hlnt_iv_load.bringToFront()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val hasCameraPermission = checkSelfPermission(Manifest.permission.CAMERA)
            Log.d(TAG, "has camera permission: $hasCameraPermission")
            val hasRecordPermission = checkSelfPermission(Manifest.permission.RECORD_AUDIO)
            Log.d(TAG, "has record permission: $hasRecordPermission")
            val hasAudioPermission =
                checkSelfPermission(Manifest.permission.MODIFY_AUDIO_SETTINGS)
            Log.d(TAG, "has audio permission: $hasAudioPermission")
            val permissions: MutableList<String> = ArrayList()
            if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.CAMERA)
            }
            if (hasRecordPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.RECORD_AUDIO)
            }
            if (hasAudioPermission != PackageManager.PERMISSION_GRANTED) {
                permissions.add(Manifest.permission.MODIFY_AUDIO_SETTINGS)
            }
            if (!permissions.isEmpty()) {
                requestPermissions(permissions.toTypedArray(), 111)
            }
        }



        title = "Classmeet"
        val webView = findViewById<WebView>(R.id.webView)
        val defaultUrl= "https://classmeet.chiguru.tech/app"

        val data: Uri? = intent?.data
        if (data != null && data.isHierarchical) {
            webView.loadUrl(data.toString())
        }else{
            webView.loadUrl(defaultUrl)
        }

        webView.webViewClient = WebViewClient()
        webView.webChromeClient = object : WebChromeClient() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            override fun onPermissionRequest(request: PermissionRequest) {
                request.grant(request.resources)
            }

            override fun onProgressChanged(view: WebView, newProgress: Int) {
                progress_bar.progress = newProgress

                //display web page
                if (newProgress > 80){
                    hlnt_iv_load.visibility = View.GONE
                    webView.visibility = View.VISIBLE
                }
                Log.d("Progress",newProgress.toString())
            }

        }

        //time passed between two back presses.
        val timeINTERVAL = 2000
        // variable to keep track of last back press
        var mBackPressed: Long = 0

        webView!!.requestFocus()
        webView.setOnKeyListener(View.OnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK
                && event.action == MotionEvent.ACTION_UP
            ) {
                if(webView.canGoBack()) {
                    //go back in previous page
                    webView.goBack()
                    return@OnKeyListener true
                }
                else
                {
                    if (mBackPressed + timeINTERVAL > System.currentTimeMillis())
                    {   // dont consume back press and pass to super
                        return@OnKeyListener false
                    }
                    else {
                        // show hint for double back press
                        Toast.makeText(this, " Double Tap back button to exit", Toast.LENGTH_SHORT).show();
                        mBackPressed = System.currentTimeMillis();
                        return@OnKeyListener true
                    }
                }
            }
            return@OnKeyListener false

        })

        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webSettings.mediaPlaybackRequiresUserGesture = false
        webSettings.domStorageEnabled = true
        webSettings.setAppCacheMaxSize( 10 * 1024 * 1024 ); // 10MB
        webSettings.setAppCachePath(applicationContext.cacheDir.absolutePath);
        webSettings.allowFileAccess = true
        webSettings.setAppCacheEnabled(true)
        webSettings.cacheMode = WebSettings.LOAD_DEFAULT
        webSettings.userAgentString =
            "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.105 Safari/537.36"

    }

}